# PYJuanPabloIIControlPacientes

*********************************************
**       Pediatria, Juan Pablo II          **
**       Control de pacientes              **
*********************************************
BY Gustavo Pereira

Desarrollar una aplicación web, que permita llevar el control de los pacientes vistos por el pediatra en turno, tanto desarrollando, una consulta llevar la historia clínica, y poder ingresar los datos de los exámenes, incluyendo un formulario para las fórmulas que son muy extensas, también control de vacunas, tiene que lograr guardar la informacion de cada paciente, y poder presentarlas en una forma práctica y simple. 

*********************************************
**      Estructura                         **
*********************************************

un - significa carpeta, mas de - es una subcarpeta del anterior

#Proyecto Hospital_Juan_Pablo_II  
-App  
--Http  
---Contollers  
----Auth  
-----DoctorController.php  
-----PacienteController.php  
-----CitaController.php  
-----HistorialController.php  
-----FormulasController.php  
-----CuentasController.php  
--Providers  
---Doctor.php  
---Paciente.php  
---Cita.php  
---Historial.php  
---Formulas.php  
---Cuentas.php  
-Bootstrop  
--cache  
---gitignore  
---package.php  
---service.php  
--app.php  
-config  
--view.php  
-public  
--css  
---imagenes_css  
----fondo_web.png  
---app.css  
---doctor.css  
---paciente.css  
---cita.css  
---banner.css  
---general.css  
--imagen  
---bases  
----fondo1.png  
----fondo2.png  
----fondomc.png  
---logo  
----logo1.png  
----logo2.png  
---doctores  
----fotoxEsthefany.png  
----fotoxJessica.png  
----fotoxElias.png  
----fotoxJerico.png  
----fotoxPonce.png  
----fotoxMatiu.png  
----fotoxCargadadesdeBD.png  
---Paciente  
----fotoPxCargadadesdeBD.png  
---Banner  
----Aviso1.png  
----Aviso2.png  
----Aviso3.png  
----Aviso4.png  
--js  
----app.js  
----banner.js  
--index.php  
--web.config  
-resources  
--js  
---app.js  
---bootstrpa.js  
--views  
---Doctor  
----create.blade.php  
----edit.blade.php  
----index.blade.php  
----show.blade.php  
---paciente  
----create.blade.php  
----edit.blade.php  
----index.blade.php  
----show.blade.php  
---cita  
----create.blade.php  
----edit.blade.php  
----index.blade.php  
----show.blade.php  
---historial  
----create.blade.php  
----edit.blade.php  
----index.blade.php  
----show.blade.php  
---formulas  
----create.blade.php  
----edit.blade.php  
----index.blade.php  
----show.blade.php  
---cuentas  
----create.blade.php  
----edit.blade.php  
----index.blade.php  
----show.blade.php  
---master  
----datos.blade.php  
---modo_Administrador  
----administrador.blade.php  
---pagina  
----portada.blade.php  
----index.blade.php  
----login.bladel.php  
--routes  
---web.php  
--vendor  
---laravel.  

